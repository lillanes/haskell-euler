factor :: Int -> Int -> Bool
factor x y = (mod y x) == 0

relevant :: [Int] -> [Int]
relevant [] = []
relevant (x:xs) = x : (relevant (clean x xs))

clean :: Int -> [Int] -> [Int]
clean _ [] = []
clean x (y:ys) = if factor y x
                 then clean x ys
                 else y : (clean x ys)

evenlyDivisible :: [Int] -> Int -> Bool
evenlyDivisible [] _ = True
evenlyDivisible (y:ys) x = if not (factor y x)
                           then False
                           else evenlyDivisible ys x

interesting :: [Int] -> [Int]
interesting xs = filter (evenlyDivisible (relevant [20,19..1])) xs

test :: Int
test = head (interesting [2520,5040..])
