palindromic :: Int -> Bool
palindromic x = palindromic' (show x)

palindromic' :: String -> Bool
palindromic' x =
    if length x <= 1
    then True
    else if (head x) == (last x)
         then palindromic' (init (tail x))
         else False

productsOfThreeDigits :: [Int]
productsOfThreeDigits = [ x * y | x <- threeDigits, y <- threeDigits ]

threeDigits :: [Int]
threeDigits = [100..999]

palindromes :: [Int] -> [Int]
palindromes xs = filter palindromic xs

test :: Int
test = last (palindromes productsOfThreeDigits)
