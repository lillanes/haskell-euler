primeSum :: Int -> Int
primeSum x = sieve (ceiling $ sqrt (fromIntegral x)) [2..x]

sieve :: Int -> [Int] -> Int
sieve s (x:xs) = 
    if s > x
    then x + sieve s [ y | y <- xs, (mod y x) /= 0 ]
    else x + (sum xs)

test :: Int
test = primeSum 2000000
