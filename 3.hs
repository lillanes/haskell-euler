factor :: Int -> Int -> Bool
factor x y = (mod y x) == 0

primes :: Int -> [Int]
primes x = sieve [2..x]

sieve :: [Int] -> [Int]
sieve [x] = [x]
sieve (x:xs) = x:(sieve (removePowers x xs))

removePowers :: Int -> [Int] -> [Int]
removePowers x xs = [y | y <- xs, not (factor x y)]

primeFactors :: Int -> [Int]
primeFactors x = [ y | y <- primes (ihalve x), factor y x]

ihalve :: Int -> Int
ihalve x = floor ((fromIntegral x) / 2)

--test :: Int
--test = last $ primeFactors 600851475143

primality :: Int -> Bool
primality x = not $ or [factor y x | y <- [2..(x-1)]]

primeFactors' :: Int -> [Int]
primeFactors' x = [y | y <- [ihalve x,((ihalve x) - 1)..2],
                        primality y, factor y x]

primeFactors'' :: Int -> [Int]
primeFactors'' 1 = []
primeFactors'' x = (firstFactor x) : (primeFactors'' (simplify x (firstFactor x)))

simplify :: Int -> Int -> Int
simplify x y = if factor y x
               then simplify (quot x y) y
               else x

firstFactor :: Int -> Int
firstFactor x = firstFactor' x (primes (ihalve x))

firstFactor' :: Int -> [Int] -> Int
firstFactor' x [] = x
firstFactor' x xs = 
    if primality x
    then x
    else
        if factor (head xs) x
        then head xs
        else firstFactor' x (tail xs)

lastFactor :: Int -> Int
lastFactor x = last $ primeFactors'' x

test :: Int
test = lastFactor 600851475143
