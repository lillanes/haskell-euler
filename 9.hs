triplets :: Int -> [(Int,Int,Int)]
triplets x = [ (a,b,x-a-b) | a <- [1..x], b <- [a..x], x-a-b > 0]

pythagoras :: (Int,Int,Int) -> Bool
pythagoras (a,b,c) = (a ^ 2) + (b ^ 2) == (c ^ 2)

tripleProduct :: (Int,Int,Int) -> Int
tripleProduct (a,b,c) = a*b*c

test :: Int
test = tripleProduct (head (filter pythagoras (triplets 1000)))
