square :: Int -> Int
square x = x ^ 2

sumOfSquares :: Int -> Int
sumOfSquares x = sum (map square [1..x])

squareOfSum :: Int -> Int
squareOfSum x = square (sum [1..x])

diff :: Int -> Int
diff x = (squareOfSum x) - (sumOfSquares x)

test :: Int
test = diff 100
