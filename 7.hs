factor :: Int -> Int -> Bool
factor x y = (mod y x) == 0

primes :: Int -> [Int]
primes x = sieve [2..x]

sieve :: [Int] -> [Int]
sieve [] = []
sieve (x:xs) = x : (sieve [ y | y <- xs, not (factor x y)])

allPrimes :: [Int]
allPrimes = sieve [2..]

prime :: Int -> Int
prime x = allPrimes !! x

test :: Int
test = prime 10001
