test :: Int
test = sum (evenFibUpTo 4000000)

evenFibUpTo :: Int -> [Int]
evenFibUpTo x = [y | y <- fibUpTo x, even y]

fibUpTo :: Int -> [Int]
fibUpTo 0 = []
fibUpTo x = (nextUpTo [1,1] x)

nextUpTo :: [Int] -> Int -> [Int]
nextUpTo xs x = if (next xs) > x
                then xs
                else nextUpTo ((next xs):xs) x

next :: [Int] -> Int
next xs = (head xs) + (head (tail xs))
