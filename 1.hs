l :: Int -> [Int]
l x = [y | y <- [1..(x - 1)], useful y]

useful :: Int -> Bool
useful x = (divisible x 3) || (divisible x 5)

divisible :: Int -> Int -> Bool
divisible x y = (mod x y) == 0

s :: Int -> Int
s x = sum (l x)

test :: Int
test = sum (l 1000)
